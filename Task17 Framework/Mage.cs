﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    public class Mage : Character
    {
        public string className;

        public Mage()
        {
            base.Resource = "Mana";
            base.ResourceAmount = 800;
            base.Armor = 20;
            base.Hp = 100;
            base.AttackType = "Ranged/Caster";
            className = "Mage";
        }
    }
}

