﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    public class Warrior : Character
    {
        public string className;

        public Warrior()
        {
            base.Resource = "Rage";
            base.ResourceAmount = 100;
            base.AttackType = "Melee";
            className = "Warrior";
        }
    }
}

