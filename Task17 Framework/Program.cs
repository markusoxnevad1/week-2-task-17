﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task17.Subtype;

namespace Task17
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Example of character creation when the user creates a Warrior with subtype Fury
            Console.WriteLine("Type in character name");
            string charName = Console.ReadLine();
            Console.WriteLine();
            Fury newchar = new Fury();
            Console.WriteLine("Name: " + (newchar.Name = charName));
            Console.WriteLine("Class: " + newchar.className);
            Console.WriteLine("Type: " + newchar.subtypeName);
            Console.WriteLine(newchar.Resource + ": " + newchar.ResourceAmount);
            Console.WriteLine("Attacktype: " + newchar.AttackType);
            Console.WriteLine("Health: " + newchar.Hp);
            Console.WriteLine("Armor: " + newchar.Armor);
            Console.WriteLine("Specialability: " + newchar.SpecialAbility);
        }
    }
}

