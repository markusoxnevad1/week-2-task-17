﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Task17.Subtype
{
    public class Assassin : Rogue
    {
        public string subtypeName;

        public Assassin()
        {
            base.SpecialAbility = "Can craft poisons";
            subtypeName = "Assassin";
        }
    }
}

