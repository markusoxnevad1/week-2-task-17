﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17.Subtype
{
    public class Frost : Mage
    {
        public string subtypeName;

        public Frost()
        {
            base.SpecialAbility = "Can freeze it's foes";
            subtypeName = "Frost";
        }
    }
}

