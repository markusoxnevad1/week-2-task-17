﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17.Subtype
{
    public class Protection : Warrior
    {
        public string subtypeName;

        public Protection()
        {
            base.SpecialAbility = "Wields a shield, granting more armor";
            base.Hp = 250;
            base.Armor = 200;
            subtypeName = "Protection";
        }
    }
}

