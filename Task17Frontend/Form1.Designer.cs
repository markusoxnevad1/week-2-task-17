﻿namespace Task17Frontend
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputName = new System.Windows.Forms.TextBox();
            this.inputHP = new System.Windows.Forms.TextBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.radioRogue = new System.Windows.Forms.RadioButton();
            this.radioMage = new System.Windows.Forms.RadioButton();
            this.radioWarrior = new System.Windows.Forms.RadioButton();
            this.panelClass = new System.Windows.Forms.Panel();
            this.labelClass = new System.Windows.Forms.Label();
            this.panelSubtype = new System.Windows.Forms.Panel();
            this.labelSpecialization = new System.Windows.Forms.Label();
            this.radioSubtypeOption1 = new System.Windows.Forms.RadioButton();
            this.radioSubtypeOption2 = new System.Windows.Forms.RadioButton();
            this.textSummary = new System.Windows.Forms.RichTextBox();
            this.inputResourceAmount = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelSummary = new System.Windows.Forms.Label();
            this.labelUpdateChar = new System.Windows.Forms.Label();
            this.pictureBoxClass = new System.Windows.Forms.PictureBox();
            this.pictureBoxSpecialization = new System.Windows.Forms.PictureBox();
            this.labelCharacterCreation = new System.Windows.Forms.Label();
            this.labelErrorInputFormat = new System.Windows.Forms.Label();
            this.dropDownList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonDeleteChar = new System.Windows.Forms.Button();
            this.labelUpdate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelClass.SuspendLayout();
            this.panelSubtype.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialization)).BeginInit();
            this.SuspendLayout();
            // 
            // inputName
            // 
            this.inputName.Location = new System.Drawing.Point(876, 483);
            this.inputName.Name = "inputName";
            this.inputName.Size = new System.Drawing.Size(149, 26);
            this.inputName.TabIndex = 1;
            // 
            // inputHP
            // 
            this.inputHP.Location = new System.Drawing.Point(876, 570);
            this.inputHP.Name = "inputHP";
            this.inputHP.Size = new System.Drawing.Size(149, 26);
            this.inputHP.TabIndex = 4;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(842, 620);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(230, 64);
            this.buttonUpdate.TabIndex = 6;
            this.buttonUpdate.Text = "Update selected character";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(135, 297);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(439, 64);
            this.buttonCreate.TabIndex = 7;
            this.buttonCreate.Text = "Create Character";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.ButtonCreate_Click);
            // 
            // radioRogue
            // 
            this.radioRogue.AutoSize = true;
            this.radioRogue.Location = new System.Drawing.Point(13, 38);
            this.radioRogue.Name = "radioRogue";
            this.radioRogue.Size = new System.Drawing.Size(82, 24);
            this.radioRogue.TabIndex = 8;
            this.radioRogue.TabStop = true;
            this.radioRogue.Text = "Rogue";
            this.radioRogue.UseVisualStyleBackColor = true;
            this.radioRogue.CheckedChanged += new System.EventHandler(this.RadioRogue_CheckedChanged);
            // 
            // radioMage
            // 
            this.radioMage.AutoSize = true;
            this.radioMage.Location = new System.Drawing.Point(13, 98);
            this.radioMage.Name = "radioMage";
            this.radioMage.Size = new System.Drawing.Size(74, 24);
            this.radioMage.TabIndex = 10;
            this.radioMage.TabStop = true;
            this.radioMage.Text = "Mage";
            this.radioMage.UseVisualStyleBackColor = true;
            this.radioMage.CheckedChanged += new System.EventHandler(this.RadioMage_CheckedChanged);
            // 
            // radioWarrior
            // 
            this.radioWarrior.AutoSize = true;
            this.radioWarrior.Location = new System.Drawing.Point(13, 68);
            this.radioWarrior.Name = "radioWarrior";
            this.radioWarrior.Size = new System.Drawing.Size(85, 24);
            this.radioWarrior.TabIndex = 11;
            this.radioWarrior.TabStop = true;
            this.radioWarrior.Text = "Warrior";
            this.radioWarrior.UseVisualStyleBackColor = true;
            this.radioWarrior.CheckedChanged += new System.EventHandler(this.RadioWarrior_CheckedChanged);
            // 
            // panelClass
            // 
            this.panelClass.Controls.Add(this.labelClass);
            this.panelClass.Controls.Add(this.radioRogue);
            this.panelClass.Controls.Add(this.radioWarrior);
            this.panelClass.Controls.Add(this.radioMage);
            this.panelClass.Location = new System.Drawing.Point(135, 112);
            this.panelClass.Name = "panelClass";
            this.panelClass.Size = new System.Drawing.Size(161, 164);
            this.panelClass.TabIndex = 12;
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(50, 11);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(48, 20);
            this.labelClass.TabIndex = 22;
            this.labelClass.Text = "Class";
            // 
            // panelSubtype
            // 
            this.panelSubtype.Controls.Add(this.labelSpecialization);
            this.panelSubtype.Controls.Add(this.radioSubtypeOption1);
            this.panelSubtype.Controls.Add(this.radioSubtypeOption2);
            this.panelSubtype.Location = new System.Drawing.Point(335, 112);
            this.panelSubtype.Name = "panelSubtype";
            this.panelSubtype.Size = new System.Drawing.Size(239, 164);
            this.panelSubtype.TabIndex = 13;
            // 
            // labelSpecialization
            // 
            this.labelSpecialization.AutoSize = true;
            this.labelSpecialization.Location = new System.Drawing.Point(61, 11);
            this.labelSpecialization.Name = "labelSpecialization";
            this.labelSpecialization.Size = new System.Drawing.Size(107, 20);
            this.labelSpecialization.TabIndex = 23;
            this.labelSpecialization.Text = "Specialization";
            // 
            // radioSubtypeOption1
            // 
            this.radioSubtypeOption1.AutoSize = true;
            this.radioSubtypeOption1.Location = new System.Drawing.Point(18, 48);
            this.radioSubtypeOption1.Name = "radioSubtypeOption1";
            this.radioSubtypeOption1.Size = new System.Drawing.Size(184, 24);
            this.radioSubtypeOption1.TabIndex = 8;
            this.radioSubtypeOption1.TabStop = true;
            this.radioSubtypeOption1.Text = "radioSubtypeOption1";
            this.radioSubtypeOption1.UseVisualStyleBackColor = true;
            this.radioSubtypeOption1.CheckedChanged += new System.EventHandler(this.RadioSubtypeOption1_CheckedChanged);
            // 
            // radioSubtypeOption2
            // 
            this.radioSubtypeOption2.AutoSize = true;
            this.radioSubtypeOption2.Location = new System.Drawing.Point(18, 78);
            this.radioSubtypeOption2.Name = "radioSubtypeOption2";
            this.radioSubtypeOption2.Size = new System.Drawing.Size(184, 24);
            this.radioSubtypeOption2.TabIndex = 11;
            this.radioSubtypeOption2.TabStop = true;
            this.radioSubtypeOption2.Text = "radioSubtypeOption2";
            this.radioSubtypeOption2.UseVisualStyleBackColor = true;
            this.radioSubtypeOption2.CheckedChanged += new System.EventHandler(this.RadioSubtypeOption2_CheckedChanged);
            // 
            // textSummary
            // 
            this.textSummary.Location = new System.Drawing.Point(1258, 79);
            this.textSummary.Name = "textSummary";
            this.textSummary.Size = new System.Drawing.Size(490, 266);
            this.textSummary.TabIndex = 14;
            this.textSummary.Text = "";
            // 
            // inputResourceAmount
            // 
            this.inputResourceAmount.Location = new System.Drawing.Point(876, 525);
            this.inputResourceAmount.Name = "inputResourceAmount";
            this.inputResourceAmount.Size = new System.Drawing.Size(149, 26);
            this.inputResourceAmount.TabIndex = 3;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(724, 482);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(51, 20);
            this.labelName.TabIndex = 15;
            this.labelName.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(724, 570);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 20);
            this.label2.TabIndex = 16;
            this.label2.Text = "HP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(724, 526);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 20);
            this.label5.TabIndex = 19;
            this.label5.Text = "Resource amount";
            // 
            // labelSummary
            // 
            this.labelSummary.AutoSize = true;
            this.labelSummary.Location = new System.Drawing.Point(1412, 35);
            this.labelSummary.Name = "labelSummary";
            this.labelSummary.Size = new System.Drawing.Size(76, 20);
            this.labelSummary.TabIndex = 20;
            this.labelSummary.Text = "Summary";
            // 
            // labelUpdateChar
            // 
            this.labelUpdateChar.AutoSize = true;
            this.labelUpdateChar.Location = new System.Drawing.Point(724, 432);
            this.labelUpdateChar.Name = "labelUpdateChar";
            this.labelUpdateChar.Size = new System.Drawing.Size(136, 20);
            this.labelUpdateChar.TabIndex = 21;
            this.labelUpdateChar.Text = "Update Character";
            // 
            // pictureBoxClass
            // 
            this.pictureBoxClass.Location = new System.Drawing.Point(657, 112);
            this.pictureBoxClass.Name = "pictureBoxClass";
            this.pictureBoxClass.Size = new System.Drawing.Size(215, 192);
            this.pictureBoxClass.TabIndex = 24;
            this.pictureBoxClass.TabStop = false;
            // 
            // pictureBoxSpecialization
            // 
            this.pictureBoxSpecialization.Location = new System.Drawing.Point(932, 112);
            this.pictureBoxSpecialization.Name = "pictureBoxSpecialization";
            this.pictureBoxSpecialization.Size = new System.Drawing.Size(215, 192);
            this.pictureBoxSpecialization.TabIndex = 25;
            this.pictureBoxSpecialization.TabStop = false;
            // 
            // labelCharacterCreation
            // 
            this.labelCharacterCreation.AutoSize = true;
            this.labelCharacterCreation.Location = new System.Drawing.Point(72, 55);
            this.labelCharacterCreation.Name = "labelCharacterCreation";
            this.labelCharacterCreation.Size = new System.Drawing.Size(161, 20);
            this.labelCharacterCreation.TabIndex = 26;
            this.labelCharacterCreation.Text = "Create new character";
            // 
            // labelErrorInputFormat
            // 
            this.labelErrorInputFormat.AutoSize = true;
            this.labelErrorInputFormat.BackColor = System.Drawing.Color.Red;
            this.labelErrorInputFormat.Location = new System.Drawing.Point(872, 701);
            this.labelErrorInputFormat.Name = "labelErrorInputFormat";
            this.labelErrorInputFormat.Size = new System.Drawing.Size(149, 20);
            this.labelErrorInputFormat.TabIndex = 27;
            this.labelErrorInputFormat.Text = "Error in input format";
            // 
            // dropDownList
            // 
            this.dropDownList.FormattingEnabled = true;
            this.dropDownList.Location = new System.Drawing.Point(124, 522);
            this.dropDownList.Name = "dropDownList";
            this.dropDownList.Size = new System.Drawing.Size(528, 28);
            this.dropDownList.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(184, 487);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 34;
            this.label1.Text = "Class";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(501, 487);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(343, 487);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 20);
            this.label8.TabIndex = 35;
            this.label8.Text = "Specialization";
            // 
            // buttonDeleteChar
            // 
            this.buttonDeleteChar.Location = new System.Drawing.Point(219, 614);
            this.buttonDeleteChar.Name = "buttonDeleteChar";
            this.buttonDeleteChar.Size = new System.Drawing.Size(230, 64);
            this.buttonDeleteChar.TabIndex = 37;
            this.buttonDeleteChar.Text = "Delete selected character";
            this.buttonDeleteChar.UseVisualStyleBackColor = true;
            this.buttonDeleteChar.Click += new System.EventHandler(this.ButtonDeleteChar_Click);
            // 
            // labelUpdate
            // 
            this.labelUpdate.AutoSize = true;
            this.labelUpdate.Location = new System.Drawing.Point(81, 426);
            this.labelUpdate.Name = "labelUpdate";
            this.labelUpdate.Size = new System.Drawing.Size(146, 20);
            this.labelUpdate.TabIndex = 38;
            this.labelUpdate.Text = "Update a character";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 487);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1840, 787);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelUpdate);
            this.Controls.Add(this.buttonDeleteChar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dropDownList);
            this.Controls.Add(this.labelErrorInputFormat);
            this.Controls.Add(this.labelCharacterCreation);
            this.Controls.Add(this.pictureBoxSpecialization);
            this.Controls.Add(this.pictureBoxClass);
            this.Controls.Add(this.labelUpdateChar);
            this.Controls.Add(this.labelSummary);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textSummary);
            this.Controls.Add(this.panelSubtype);
            this.Controls.Add(this.panelClass);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.inputHP);
            this.Controls.Add(this.inputResourceAmount);
            this.Controls.Add(this.inputName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelClass.ResumeLayout(false);
            this.panelClass.PerformLayout();
            this.panelSubtype.ResumeLayout(false);
            this.panelSubtype.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialization)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputName;
        private System.Windows.Forms.TextBox inputHP;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.RadioButton radioRogue;
        private System.Windows.Forms.RadioButton radioMage;
        private System.Windows.Forms.RadioButton radioWarrior;
        private System.Windows.Forms.Panel panelClass;
        private System.Windows.Forms.Panel panelSubtype;
        private System.Windows.Forms.RadioButton radioSubtypeOption1;
        private System.Windows.Forms.RadioButton radioSubtypeOption2;
        private System.Windows.Forms.RichTextBox textSummary;
        private System.Windows.Forms.TextBox inputResourceAmount;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelSpecialization;
        private System.Windows.Forms.Label labelSummary;
        private System.Windows.Forms.Label labelUpdateChar;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.PictureBox pictureBoxClass;
        private System.Windows.Forms.PictureBox pictureBoxSpecialization;
        private System.Windows.Forms.Label labelCharacterCreation;
        private System.Windows.Forms.Label labelErrorInputFormat;
        private System.Windows.Forms.ComboBox dropDownList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonDeleteChar;
        private System.Windows.Forms.Label labelUpdate;
        private System.Windows.Forms.Label label3;
    }
}

