﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task17;
using Task17.Subtype;
using System.Data.SQLite;


namespace Task17Frontend
{
    public partial class Form1 : Form
    {
        public Character Char { get; set; }
        public List<(int, Character)> gameCharacters { get; set; }
        public List<string> dropDownContent;


        public Form1()
        {
            InitializeComponent();

            radioRogue.Checked = true;
            radioSubtypeOption1.Checked = true;
            ClassRadioChange();
            labelErrorInputFormat.Visible = false;

            #region fonts
            // Change some of the fonts away from the default
            textSummary.Font = new Font("Courier New", 10, FontStyle.Bold);
            labelCharacterCreation.Font = new Font("Courier New", 14, FontStyle.Bold);
            labelSummary.Font = new Font("Courier New", 11, FontStyle.Bold); 
            labelUpdate.Font = new Font("Courier New", 14, FontStyle.Bold);
            labelUpdateChar.Font = new Font("Courier New", 11, FontStyle.Bold);
            dropDownList.Font = new Font("Courier New", 8, FontStyle.Bold);
            #endregion


            // Get data from database
            using (SQLiteConnection conn = CreateConnection())
            {
                (List<(int, Character)>, List<string>) resultList = ReadData(conn);

                gameCharacters = resultList.Item1;
                dropDownContent = resultList.Item2;

                dropDownList.DataSource = dropDownContent;
                conn.Close();
            }
        }

        // Create character, display summary and write insert-query to file
        private void ButtonCreate_Click(object sender, EventArgs e)
        {

            if (labelErrorInputFormat.Visible == true) { labelErrorInputFormat.Visible = false; }

            if (radioMage.Checked)
            {
                radioSubtypeOption1.Text = "Fire";
                radioSubtypeOption2.Text = "Frost";


                if (radioSubtypeOption1.Checked)                            // Fire
                {
                    Char = new Fire();
                }
                else if(radioSubtypeOption2.Checked)                        // Frost
                {
                    Char = new Frost();
                }
            }
            else if(radioRogue.Checked)
            {
                radioSubtypeOption1.Text = "Assassin";
                radioSubtypeOption2.Text = "Combat";

                if (radioSubtypeOption1.Checked)                            // Assassin
                {
                    Char = new Assassin();
                }
                else if (radioSubtypeOption2.Checked)                       // Combat
                {
                    Char = new Combat();
                }
            }
            else if(radioWarrior.Checked)
            {
                radioSubtypeOption1.Text = "Fury";
                radioSubtypeOption2.Text = "Protection";

                if (radioSubtypeOption1.Checked)                            // Fury
                {
                    Char = new Fury();
                }
                else if (radioSubtypeOption2.Checked)                       // Protection
                {
                    Char = new Protection();
                } 
            }

            DisplaySummary();                       
            WriteToFile(CharacterToString());                               // Write summary to file (task requirement)                                      
            WriteToQueryFile(GetInsertCharacterQuery());                    // Write query to file   (task requirement)


            // Insert new character into database and update local data. A bit redundant to get info from db. 
            using (SQLiteConnection conn = CreateConnection())
            {
                AlterData(conn, GetInsertCharacterQuery());

                (List<(int, Character)>, List<string>) resultList = ReadData(conn);

                gameCharacters = resultList.Item1;
                dropDownContent = resultList.Item2;

                dropDownList.DataSource = dropDownContent;
                conn.Close();
            }
        }


        // Sets images and text based on radio-button choices
        public void ClassRadioChange()
        {
            string classImagePath = "";
            string specializationImagePath = "";

            if (radioMage.Checked)
            {
                radioSubtypeOption1.Text = "Fire";
                radioSubtypeOption2.Text = "Frost";

                classImagePath = "img\\mage.png";

                if (radioSubtypeOption1.Checked)                            // Fire
                {
                    specializationImagePath = "img\\fire.png";
                }
                else if (radioSubtypeOption2.Checked)                        // Frost
                {

                    specializationImagePath = "img\\frost.png";
                }
            }
            else if (radioRogue.Checked)
            {
                radioSubtypeOption1.Text = "Assassin";
                radioSubtypeOption2.Text = "Combat";

                classImagePath = "img\\rogue.png";

                if (radioSubtypeOption1.Checked)                            // Assassin
                {
                    specializationImagePath = "img\\assassin.png";
                }
                else if (radioSubtypeOption2.Checked)                       // Combat
                {
                    specializationImagePath = "img\\combat.png";
                }
            }
            else if (radioWarrior.Checked)
            {
                radioSubtypeOption1.Text = "Fury";
                radioSubtypeOption2.Text = "Protection";

                classImagePath = "img\\warrior.png";

                if (radioSubtypeOption1.Checked)                            // Fury
                {
                    specializationImagePath = "img\\fury.png";
                }
                else if (radioSubtypeOption2.Checked)                       // Protection
                {
                    specializationImagePath = "img\\protection.png";
                }
            }

            pictureBoxClass.ImageLocation = classImagePath;                         // Display class picture
            pictureBoxClass.SizeMode = PictureBoxSizeMode.Zoom;

            pictureBoxSpecialization.ImageLocation = specializationImagePath;       // Display specialization picture
            pictureBoxSpecialization.SizeMode = PictureBoxSizeMode.Zoom;
        }


        // Update Character values in database from UI
        private void ButtonUpdate_Click(object sender, EventArgs e)
        {

            if (dropDownList.SelectedItem != null && gameCharacters != null)
            {

                (int, Character) charWithIndex = gameCharacters[dropDownList.SelectedIndex];
                Character selectedChar = charWithIndex.Item2;

                try
                {
                    if (inputName.Text != "") { selectedChar.Name = inputName.Text; }
                    if (inputHP.Text != "") { selectedChar.Hp = Int32.Parse(inputHP.Text); }
                    if (inputResourceAmount.Text != "") { selectedChar.ResourceAmount = Int32.Parse(inputResourceAmount.Text); }

                    string updateString =   $"UPDATE CHARACTERS"
                                          + $"  SET NAME = \"{selectedChar.Name}\","
                                          + $"  HP = {selectedChar.Hp},"
                                          + $"  RESOURCEAMOUNT = {selectedChar.ResourceAmount}"
                                          + $"  WHERE"
                                          + $"  ID = {charWithIndex.Item1}; ";


                    using (SQLiteConnection conn = CreateConnection())
                    {
                        AlterData(conn, updateString);
                        (List<(int, Character)>, List<string>) resultList = ReadData(conn);

                        gameCharacters = resultList.Item1;
                        dropDownContent = resultList.Item2;

                        dropDownList.DataSource = dropDownContent;
                        conn.Close();   
                    }
                }
                catch (Exception ex)
                {
                    labelErrorInputFormat.Visible = true;
                    
                }
                finally
                {
                    inputName.Text = "";
                    inputHP.Text = "";
                    inputResourceAmount.Text = "";
                }
            }
        }

        // Delete the character that is selected in the drop down menu
        private void ButtonDeleteChar_Click(object sender, EventArgs e)
        {
            if (dropDownList.SelectedItem != null && gameCharacters != null)
            {

                (int, Character) charWithIndex = gameCharacters[dropDownList.SelectedIndex];
                
                string updateString = $"DELETE FROM CHARACTERS WHERE ID = {charWithIndex.Item1}";

                using (SQLiteConnection conn = CreateConnection())
                {
                    AlterData(conn, updateString);

                    // Update local data
                    (List<(int, Character)>, List<string>) resultList = ReadData(conn);
                    gameCharacters = resultList.Item1;
                    dropDownContent = resultList.Item2;
                    dropDownList.DataSource = dropDownContent;
                    conn.Close();
                }
            }
        }


        // Generate a string to summarize the Character
        private string CharacterToString()
        {

            string summary = "";

            if(Char == null) { return summary; }

            if (Char is Mage mage)
            {
                summary += "Class: " + mage.className;

                if (Char is Frost frost)
                {
                    summary += "\nSpecialization: " + frost.subtypeName;
                }
                else if (Char is Fire fire)
                {
                    summary += "\nSpecialization: " + fire.subtypeName;
                }
            }
            else if (Char is Rogue rogue)
            {
                summary += "Class: " + rogue.className;

                if (Char is Assassin assassin)
                {
                    summary += "\nSpecialization: " + assassin.subtypeName;
                }
                else if (Char is Combat combat)
                {
                    summary += "\nSpecialization: " + combat.subtypeName;
                }
            }
            else if (Char is Warrior warrior)
            {
                summary += "Class: " + warrior.className;

                if (Char is Fury fury)
                {
                    summary += "\nSpecialization: " + fury.subtypeName;
                }
                else if (Char is Protection protection)
                {
                    summary += "\nSpecialization: " + protection.subtypeName;
                }
            }
            summary += "\n";
            summary += "\n" + "Name: " + Char.Name;
            summary += "\n" + $"{Char.Resource}: " + Char.ResourceAmount;
            summary += "\n" + "HP: " + Char.Hp;
            summary += "\n" + "Armor: " + Char.Armor;
            summary += "\n" + "Attack type: " + Char.AttackType;
            summary += "\n" + "Special ability: " + Char.SpecialAbility;

            return summary;
        }

        // Create insert query for the current Character. TOOD - refactor with CharacterToString?
        private string GetInsertCharacterQuery()
        {
            string className = "";
            string specializationName = "";

            if (Char == null) { return ""; }

            if (Char is Mage mage)
            {
                className  = mage.className;

                if (Char is Frost frost)
                {
                    specializationName = frost.subtypeName;
                }
                else if (Char is Fire fire)
                {
                    specializationName = fire.subtypeName;
                }
            }
            else if (Char is Rogue rogue)
            {
                className  =  rogue.className;

                if (Char is Assassin assassin)
                {
                    specializationName = assassin.subtypeName;
                }
                else if (Char is Combat combat)
                {
                    specializationName = combat.subtypeName;
                }
            }
            else if (Char is Warrior warrior)
            {
                className = warrior.className;

                if (Char is Fury fury)
                {
                    specializationName = fury.subtypeName;
                }
                else if (Char is Protection protection)
                {
                    specializationName = protection.subtypeName;
                }
            }
         
            string name = Char.Name;
            string resource = Char.Resource;
            int resourceAmount = Char.ResourceAmount;
            int hp = Char.Hp;
            int armor  = Char.Armor;
            string attackType = Char.AttackType;
            string specialAbility = Char.SpecialAbility;         

            return $"INSERT INTO CHARACTERS(CLASSNAME, SPECIALIZATION, NAME, RESOURCE, RESOURCEAMOUNT, HP, ARMOR, ATTACKTYPE, SPECIALABILITY)" +
                                   $"VALUES(\"{className}\",\"{specializationName}\",\"{name}\",\"{resource}\",{resourceAmount},{hp},{armor},\"{attackType}\",\"{specialAbility}\"); ";
        }

        private void DisplaySummary()
        {
            if (Char != null)
            {
                textSummary.Text = CharacterToString();
            }
        }

        public void WriteToFile(string text)
        {
            using (StreamWriter sw = new StreamWriter("summary.txt"))
            {
                sw.Write(text);
            }
        }
        
        public void WriteToQueryFile(string queries)
        {
            using (StreamWriter sw = new StreamWriter("CharacterInsertSQL.txt"))
            {
                sw.Write(queries);
            }
        }

        private static string FormatSpaces(string s)
        {
            string spaceString = "";

            if(s.Length < 4)
            {
                for (int i = 0; i < 6 - s.Length; i++)
                {
                    spaceString += " ";
                }
            }
            else if(s.Length < 15)
            {
                for (int i = 0; i < 15 - s.Length; i++)
                {
                    spaceString += " ";
                }
            }
            
            return spaceString;
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            radioSubtypeOption1.Checked = false;
            radioSubtypeOption2.Checked = false;
        }

        private void RadioRogue_CheckedChanged(object sender, EventArgs e)
        {
            ClassRadioChange();
        }

        private void RadioWarrior_CheckedChanged(object sender, EventArgs e)
        {
            ClassRadioChange();
        }

        private void RadioMage_CheckedChanged(object sender, EventArgs e)
        {
            ClassRadioChange();
        }

        private void RadioSubtypeOption1_CheckedChanged(object sender, EventArgs e)
        {
            ClassRadioChange();
        }

        private void RadioSubtypeOption2_CheckedChanged(object sender, EventArgs e)
        {
            ClassRadioChange();
        }

        //Creates the connection to a specified database
        static SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqlite_conn;

            // Create a new database connection:
            sqlite_conn = new SQLiteConnection("Data Source=db.db; Version = 3; New = True; Compress = True; ");


            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return sqlite_conn;
        }

        private static void AlterData(SQLiteConnection conn, string insertQuery)
        {
            SQLiteCommand sqlite_cmd;

            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = insertQuery;
            sqlite_cmd.ExecuteNonQuery();
        }


        // This method reads all the data from the database and returns it
        // Messy returntype, but have to descope a bit. //TODO do it in a better way
        static (List<(int, Character)>, List<string>) ReadData(SQLiteConnection conn)
        {

            string query = "SELECT * FROM CHARACTERS";
            SQLiteCommand readCmd = new SQLiteCommand(query, conn);
            SQLiteDataReader reader = readCmd.ExecuteReader();

            List<(int, Character)> characters = new List<(int, Character)>();
            List<string> dropDownList_List = new List<string>();


            while (reader.Read())
            {
                Character temp;
                int id = (int) (long) reader["ID"];
                string specialization = (string)reader["SPECIALIZATION"];

                string idString = "" + id;

                // String that is displayed in the dropdown menu
                dropDownList_List.Add(idString + FormatSpaces(idString) + reader["CLASSNAME"] + FormatSpaces((string)reader["CLASSNAME"]) + reader["SPECIALIZATION"] + FormatSpaces((string)reader["SPECIALIZATION"]) + reader["NAME"]);

                switch (specialization)
                {
                    case "Fire":
                        temp = new Fire();
                        break;
                    case "Frost":
                        temp = new Frost();
                        break;
                    case "Fury":
                        temp = new Fury();
                        break;
                    case "Protection":
                        temp = new Protection();
                        break;
                    case "Assassin":
                        temp = new Assassin();
                        break;
                    case "Combat":
                        temp = new Combat();
                        break;
                    default:
                        temp = new Fire();
                        break;
                }


                temp.Name = (string)reader["NAME"];
                temp.ResourceAmount = (int) (long) reader["RESOURCEAMOUNT"];
                temp.Hp = (int) (long) reader["HP"];
                temp.Armor = (int) (long) reader["ARMOR"];
                characters.Add((id, temp));
            }

            return (characters, dropDownList_List);
        }
    }
}

