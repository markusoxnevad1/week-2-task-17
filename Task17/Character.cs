﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Character
    {
        private string name;
        private int hp;
        private int armor;
        private string resource;
        private int resourceAmount;
        private string attackType;

        public string Name { get => name; set => name = value; }
        public int Hp { get; set; }
        public int Armor { get; set; }
        public string SpecialAbility { get; set; }
        public string Resource { get; set; }
        public int ResourceAmount { get; set; }
        public string AttackType { get; set; }
        public void Attack()
        {
            Console.WriteLine("You performed an attack!");
        }
        public void Move()
        {
            Console.WriteLine("You moved");
        }

    }
}
