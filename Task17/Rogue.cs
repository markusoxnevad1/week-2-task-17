﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Rogue : Character
    {
        public string className;

        public Rogue()
        {
            base.Resource = "Energy";
            base.ResourceAmount = 100;
            base.Armor = 100;
            base.Hp = 120;
            base.AttackType = "Melee";
            className = "Rogue";
        }
    }
}
