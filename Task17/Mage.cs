﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17
{
    public class Mage : Character
    {
        public string className;

        public Mage()
        {
            base.Resource = "Mana";
            base.ResourceAmount = 800;
            base.Armor = 20;
            base.Hp = 100;
            base.AttackType = "Ranged/Caster";
            className = "Mage";
        }
    }
}
