﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Subtype
{
    public class Fury : Warrior
    {
        public string subtypeName;

        public Fury()
        {
            base.SpecialAbility = "Wields a two-handed weapon, that hits slow but hard";
            base.Hp = 160;
            base.Armor = 140;
            subtypeName = "Fury";
        }
    }
}
