﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Subtype
{
    public class Combat : Rogue
    {
        public string subtypeName;

        public Combat()
        {
            base.SpecialAbility = "Can stealth through the shadows";
            subtypeName = "Combat";
        }
    }
}
