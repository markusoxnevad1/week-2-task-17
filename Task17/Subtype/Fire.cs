﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Subtype
{
    public class Fire : Mage
    {
        public string subtypeName;

        public Fire()
        {
            base.SpecialAbility = "Can cast a huge firebolt, that deals alot of damage";
            subtypeName = "Fire";
        }
    }
}
